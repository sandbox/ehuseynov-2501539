<?php
/**
 * @file
 * Implements token2 authentication. It's like magic, except it's math.
 */

class token2 {

  public $_apiKey  = NULL;

  public $_serverTimeout = 3;

  public $lastError = NULL;
  
  public $validation = "";
  
  public $response = "";
  
  public $_server = 'https://api.token2.com';

  /**
   * Add a new token2 user and get the token2 ID.
   *
   * @param string $email
   *   User email.
   *
   * @param string $cellphone
   *   User cellphone.
   *
   * @param string $pincode
   *   User pincode.  
   *
   * @return mixed
   *   Returns the users token2 ID on success or FALSE on error.
   */
  public function userNew($email, $cellphone, $pincode,$type) {

    $data = array(
      'email'        => $email,
      'phone'    => $cellphone,
      'pin' => $pincode,
	  'type' => $type,
    );

    $result = $this->apiCall('new', $data);
	 
	  
 

    if($result === FALSE) {
      $this->lastError = 'token2_SERVER_ERROR';
      return FALSE;
    }

    if (isset($result->errors)) {
      if (isset($result->errors->api_key)) {
        $this->lastError = 'token2_SERVER_INVALID_API_KEY';
      } else {
        $this->lastError = 'token2_SERVER_INVALID_DATA';
      }
      return FALSE;
    }
if ($result->success=="true") {
    if (isset($result->userid)) {
      return $result->userid."|".$result->hash."|".$result->hashqr;
    }
	}
    $this->lastError = 'token2_SERVER_SAYS_NO';
    return FALSE;
  }

  /**
   * Verify a token2 OTP.
   *
   * @param int $token2Id
   *   User token2 ID.
   *
   * @param int $token
   *   User supplied OPT/token.
   *
   * @return boolean
   *   Return true if a valid token2 token is supplied, false on any errors.
   */
  public function verify($token2Id, $token) {
    $data = array(
      'token'    => $token,
      'token2_id' => $token2Id,
    );

    $result = $this->apiCall('verify', $data);

    if ($result === FALSE) {
      $this->lastError = 'token2_SERVER_ERROR';
      return FALSE;
    }

    if (isset($result->errors)) {
      if (isset($result->errors->message) && $result->errors->message == 'token is invalid') {
        $this->lastError = 'token2_SERVER_BAD_OTP';
      }
      elseif (isset($result->errors->api_key)) {
        $this->lastError = 'token2_SERVER_INVALID_API_KEY';
      }
      else {
        $this->lastError = 'token2_SERVER_INVALID_DATA';
      }
      return FALSE;
    }

    if (isset($result) && $result == 'true') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Request SMS token.
   *
   * @param int $token2Id
   *   token2 ID to request SMS token for.
   *
   * @return boolean
   *   Returns TRUE if SMS request was OK. False if not.
   */
  public function sendSMS($token2Id) {
    $data = array(
      'token2_id' => $token2Id,
    );

    $result = $this->apiCall('send', $data);

    if ($result === FALSE) {
      $this->lastError = 'token2_SERVER_ERROR';
      return FALSE;
    }

    if (isset($result->errors)) {
      $this->lastError = 'token2_SERVER_INVALID_DATA';
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Performs a call to the token2 API.
   *
   * @param string $action
   *   Action to perform: should be 'new' or 'verify'
   *
   * @param array $data
   *   Data to perform the action with.
   *
   * @return mixed
   *   Decoded JSON results from token2 server or FALSE on error.
   */
  private function apiCall($action, $data) {
 

    switch ($action) {
      case 'new':
        if ($data['type']=="sms") { $type=1; }
		if ($data['type']=="app") { $type=0; }
		
		$url = $this->_server.'/createuser?format=1&api='.$this->_apiKey.'&email='.$data['email']."&phone=".$data['phone']."&pin=".$data['pin']."&type=".$type;
        $postData = http_build_query($data);
		
	 
        $opts =  array(
          'method'  => 'POST',
          'timeout' => $this->_serverTimeout,
          'data' => $postData,
		    'email' => $data['email'],
			'phone' => $data['phone'],
			'pin' => $data['pin'],
			'type' => $type,
        );
        break;

      case 'verify':
        $url = $this->_server.'/validate?format=1&token='.$data['token'].'&userid='.$data['token2_id'].'&api='.$this->_apiKey.'';
 
        $opts = array(
          'method'  => 'GET',
          'timeout' => $this->_serverTimeout,
        );
        break;

		
		case 'send':
        $url = $this->_server.'/validate?format=1&send=1&userid='.$data['token2_id'].'&api='.$this->_apiKey.'';
 
        $opts = array(
          'method'  => 'GET',
          'timeout' => $this->_serverTimeout,
        );
			
		
        break;
		
       
    }
 
    $request = drupal_http_request($url, $opts);
	//var_dump($request);
	if (isset($request->error)) {
	
	form_set_error('error', "Token2 API request error: ".$request->error);
	 
	 return FALSE;
	 
	 
	} else {
	 
	 if ($action=="send" ) { 
	//Save siteid to session to build a restore link
	$response=json_decode($request->data);
	if ($response->sms_restore_mode=="1") {
	$_SESSION['token2smsrestoreid']=$response->siteid;
	$_SESSION["token2userid"]=$response->userid;
	}
	
	}
	 
if (isset($request->data)) {	 
$response=json_decode($request->data);
 } else {
 
  return FALSE;
 }
 
 
  if (isset($response->success)) {
    if ($response->success!="true" ) {
      return FALSE;
    }
} else {

 return FALSE;
}
	
	
	
	
	if ($action=="verify" ) { 
	return $response->validation;
	} else {
    return json_decode($request->data);
	}
   }
   
   }
}
