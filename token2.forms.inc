<?php
/**
 * @file
 * Module that implements two factor authentication using token2.
 */

/**
 * Form callback: token2_form_verify().
 *
 * Request a token2 token from a user.
 */
function token2_form_verify($form, &$form_state) {

  $form['#attached']['css'] = array(
    'https://www.token2.com/form.token2.min.css' => array(
      'type' => 'external',
    ),
  );
$reset="";
if (isset ($_SESSION['token2smsrestoreid']) ) {
 if (intval($_SESSION['token2smsrestoreid'])!=0) {
  
  $reset="<a target=_blank href=https://token2.com/restore?siteid=".$_SESSION['token2smsrestoreid']."&userid=".$_SESSION['token2userid'].">".t('Reset Mobile OTP App Profile')."</a>";
  
  
  }
  }
  
  $form['token2_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Token'),
    '#attributes' => array('id' => array('token2-token')),
    '#description' => t('Enter your token2 token for %application.', array(
      '!request' => url('token2/request', array(
      'query' => array('goto' => current_path()),
      )),
      '%application' => variable_get('token2_application', ''),
    )).$reset,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation function for token2_form_verify().
 */
function token2_form_verify_validate($form, &$form_state) {

  // We need the username that is logging in to load information about
  // token2 settings.
  if (isset($_SESSION['token2']['form_state']['values']['name'])) {
    $form_state['values']['name'] = $_SESSION['token2']['form_state']['values']['name'];
  }

  // We just call the token2_verify() we use for all forms since it's
  // written to work with anything.
  return token2_verify($form, $form_state);
}

/**
 * Submit function for token2_form_verify().
 */
function token2_form_verify_submit($form, &$form_state) {
  global $user;

  // Use the saved form state to pass to user_login_submit().
  // This will log in the user.
  user_login_submit($_SESSION['token2']['form'], $_SESSION['token2']['form_state']);

  // Welcome...
  drupal_set_message(t('Valid token2 token entered.'), 'status');

  module_invoke('rules', 'invoke_event', 'token2_login', $user);

  // And redirect.
  if (isset($_SESSION['token2']['destination'])) {
    $form_state['redirect'] = $_SESSION['token2']['destination'];
  }
  else {
    $form_state['redirect'] = 'user';
  }

  // We don't need this anymore.
  unset($_SESSION['token2']);
}

/**
 * Form callback: token2_form_enable().
 *
 * Enable token2 authentication for a user.
 */
function token2_form_enable($form, &$form_state, $user) {
  $form['#uid'] = $user->uid;

  $form['#attached']['css'] = array(
    'https://www.token2.com/form.token2.min.css' => array(
      'type' => 'external',
    ),
  );

  $form['#attached']['js'] = array(
    'https://www.token2.com/form.token2.min.js' => array(
      'type' => 'external',
    ),
  );

  $form['enable_token2'] = array(
    '#title' => t('Enable token2'),
    '#type' => 'textfield',
    '#description' => t('To enable token2 enter your %application token from your cellphone.', array('%application' => variable_get('token2_application', ''))),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Enable'),
  );

  return $form;
}

/**
 * Validation function for token2_form_enable().
 */
function token2_form_enable_validate($form, &$form_state) {
  $token2_id = _token2_get_token2_id($form['#uid'], TRUE);

  $token2 = _token2_get_api();
  $verification = $token2->verify($token2_id, $form_state['values']['enable_token2']);

  if($verification === FALSE) {
    form_set_error('enable_token2', t('The token you entered could not be verified.') );
  }
}

/**
 * Submit function for token2_form_enable().
 */
function token2_form_enable_submit($form, &$form_state) {
  db_update('token2')
    ->fields(array(
      'active' => 1,
    ))
    ->condition('uid', $form['#uid'], '=')
    ->execute();

  module_invoke('rules', 'invoke_event', 'token2_enabled', $form['#uid']);
  drupal_set_message(t('token2 is now enabled for your account.'));
}

/**
 * Form callback: token2_form_disable().
 *
 * Disable token2 authentication for a user.
 */
function token2_form_disable($form, &$form_state, $user) {
  $form['#uid'] = $user->uid;

  $form['#attached']['css'] = array(
    'https://www.token2.com/form.token2.min.css' => array(
      'type' => 'external',
    ),
  );

  $form['#attached']['js'] = array(
    'https://www.token2.com/form.token2.min.js' => array(
      'type' => 'external',
    ),
  );

  $form['disable_token2'] = array(
    '#title' => t('Disable token2'),
    '#type' => 'hidden',
   
    '#value' => 1,
  );

 
  $form['submit'] = array(
    '#type' => 'submit',
	 '#description' => t('Click below to disable token2 for this account'),
    '#value' => t('Disable Token2 for this account'),
  );

  return $form;
}

/**
 * Validation function for token2_form_disable().
 */
function token2_form_disable_validate($form, &$form_state) {
  $token2_id = _token2_get_token2_id($form['#uid'], TRUE);

  $token2 = _token2_get_api();
   
}

/**
 * Submit function for token2_form_disable().
 */
function token2_form_disable_submit($form, &$form_state) {
  db_update('token2')
    ->fields(array(
      'active' => 0,
    ))
    ->condition('uid', $form['#uid'], '=')
    ->execute();

  module_invoke('rules', 'invoke_event', 'token2_disabled', $form['#uid']);
  drupal_set_message(t('token2 is now disabled for your account.'), 'warning');
}

/**
 * Form callback: token2_form_unregister().
 *
 * Unregister token2 ID from user.
 */
function token2_form_unregister($form, &$form_state, $user) {
  $form['#uid'] = $user->uid;
  return confirm_form($form,
    t('Are you sure you want to unregister your token2 ID?'),
    'user/'.$user->uid.'/token2',
    t('This will clear all the token2 information connected to your user on this site, however this will not delete your token2.com account.'),
    t('Unregister ID'),
    t('Cancel')
  );
}

/**
 * Submit function for token2_form_unregister().
 */
function token2_form_unregister_submit($form, &$form_state) {
  db_delete('token2')
    ->condition('uid', $form['#uid'], '=')
    ->execute();

  module_invoke('rules', 'invoke_event', 'token2_disabled', $form['#uid']);
  $form_state['redirect'] = 'user/' . $form['#uid'] . '/token2';
}

/**
 * Form callback: token2_form_register().
 *
 * Register a token2 ID to user.
 */
function token2_form_register($form, &$form_state, $user) {
  $form['#uid'] = $user->uid;

  $form['#attached']['css'] = array(
    'https://www.token2.com/form.token2.min.css' => array(
      'type' => 'external',
    ),
  );

  $form['#attached']['js'] = array(
    'https://www.token2.com/form.token2.min.js' => array(
      'type' => 'external',
    ),
  );

  $form['newid'] = array(
    '#type' => 'fieldset',
    '#title' => t('Register your Token2 ID  '),
    '#description' => t('Please enter your mobile phone number (in full international format) and a numeric PIN code below. See !link for more details. ',
    array(
      '!link' => '<a href="http://token2.com">token2.com</a>',
    )),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['newid']['email'] = array(
    '#title' => t('E-mail'),
    '#type' => 'textfield',
    '#default_value' => $user->mail,
    '#description' => '',
    '#required' => TRUE,
    '#disabled' => TRUE,
  );

    if (variable_get('token2_allow_sms', '')==1) {
	$optarray=array(
      'sms' => t('SMS Message'),
	  'app' => t('Mobile App')
    );
	
	} else {
	
		$optarray=array(
       
	  'app' => t('Mobile App')
    );
	
	}
 
    $form['newid']['token2type'] = array(
    '#title' => t('Token type'),
    '#type' => 'select',
    '#description' => t('Select how you want to receive your token') ,
	
    '#options' => $optarray,
    '#default_value' => 'app',
    '#required' => TRUE,
  );
  
  $form['newid']['cellphone'] = array(
    '#title' => t('Mobile phone'),
    '#type' => 'textfield',
    '#attributes' => array('id' => array('token2-cellphone')),
    '#description' => '',
    '#required' => FALSE,
  );

  $form['newid']['pincode'] = array(
    '#title' => t('PIN Code'),
    '#type' => 'textfield',
    '#attributes' => array('id' => array('token2-pincode'),'size' =>'6' ),
    '#description' => '',
    '#required' => FALSE,
  );

  $form['newid']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register Token2'),
  );

  return $form;
}

/*    Validation function for token2_form_register().  */
function token2_form_register_validate($form, &$form_state) {

if ( intval($form_state['values']['pincode'])<999  ) {

 
      form_set_error('pincode', t('Please enter a PIN code (at least 4 digits)'));

} else {


  if (!empty($form_state['values']['cellphone']) && !empty($form_state['values']['pincode'])) {

    $token2 = _token2_get_api();
	 
	    if ($token2   !== FALSE) {
    $token2_user = $token2->userNew($form_state['values']['email'], $form_state['values']['cellphone'], $form_state['values']['pincode'],$form_state['values']['token2type']);
	
	} else {
	$token2_user = FALSE;
	}

    if ($token2_user !== FALSE) {
	$token2_res_arr=explode("|",$token2_user);
      $form_state['values']['token2Id'] = $token2_res_arr[0];
	  $form_state['values']['token2hash'] = $token2_res_arr[1];
	   $form_state['values']['token2hashqr'] = $token2_res_arr[2];
	  
    }
    else {
	if (isset($token2->lastError)){ $errshow=$token2->lastError; } else { $errshow=" Check API settings."; }
      watchdog('token2', 'Registration failed. %error', array('%error' => $errshow), WATCHDOG_DEBUG);
      form_set_error('cellphone', t('Unable to verify your phone number. Please check the format. This could be due to server error as well, please check again later.'));
    }
	
	}
  }
}

/**
 * Submit function for token2_form_register().
 */
function token2_form_register_submit($form, &$form_state) {
 
  if ($form_state['values']['token2Id'] > 0) {
    db_merge('token2')
      ->key(array('uid' => $form['#uid']))
      ->fields(array(
        'uid' => $form['#uid'],
        'token2_id' => $form_state['values']['token2Id'],
      ))
      ->execute();

	 //Show QR code if it is a Mobile App
if ($form_state['values']['token2type']=="app") {

$qrcode="<img   src='".$form_state['values']['token2hashqr']."'>";
$hashcode="<pre>".$form_state['values']['token2hash']."</pre>";

drupal_set_message(t('You have now registered your token2 account to this site. However token2 still needs to be activated before your account is protected.Scan the code below using Token2 Mobile Application')."<br>".$qrcode."<br>".t('Alternatively, you can enter the hash below manually.')."<br>".$hashcode );

}


if ($form_state['values']['token2type']=="sms" && variable_get('token2_allow_sms', '')==1 ) {

//Send SMS
$token2_id = _token2_get_token2_id($form['#uid'], TRUE);

  $token2 = _token2_get_api();
  $verification = $token2->sendSMS($token2_id);
  
drupal_set_message(t('You have now registered your token2 account to this site. However token2 still needs to be activated before your account is protected. Enter the OTP sent to you by Token2 below by SMS to the number you have specified. ')   );

}

	  
    
  }
}

/**
 * Form callback: token2_form_request().
 *
 * Request one time token.
 */
function token2_form_request($form, &$form_state) {
  if(isset($_GET['goto']) && drupal_valid_path($_GET['goto'])) {
    $form['#goto'] = $_GET['goto'];
  }

  $form['type'] = array(
    '#title' => t('OTP type'),
    '#type' => 'select',
    '#description' => t('Select how you want to receive your OTP'),
    '#options' => array(
      'sms' => t('SMS Message'),
	  'app' => t('Mobile App')
    ),
    '#default_value' => 'app',
    '#required' => TRUE,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Request token'));

  return $form;
}

/**
 * Submit function for token2_form_request().
 */
function token2_form_request_submit($form, &$form_state) {
  global $user;

  $token2_api = _token2_get_api();

  // If we have a session variable, this means that we are in a login process.
  // Load the user by the almost logged in username.
  if (isset($_SESSION['token2']['form_state']['values']['name'])) {
    $token2_user = user_load_by_name($_SESSION['token2']['form_state']['values']['name']);
    $token2_id = _token2_get_token2_id($token2_user->uid);
  }
  else {
    $token2_id = _token2_get_token2_id($user->uid);
  }

  switch ($form_state['values']['type']) {
    case 'sms':
      $sms = $token2_api->requestSms($token2_id);
      if ($sms === TRUE) {
        drupal_set_message(t('SMS message code sent.'));
      }
      else {
        watchdog('token2', 'SMS request failed. %error', array('%error' => $token2->lastError), WATCHDOG_ERROR);
        form_set_error('type', t('Unable to send SMS, please try again later.'));
      }
      break;
  }

  if (isset($_SESSION['token2']['destination'])) {
    $form_state['redirect'] = $_SESSION['token2']['destination'];
  }
  else {
    $form_state['redirect'] = 'user';
  }

  if(isset($form['#goto'])) {
    $form_state['redirect'] = $form['#goto'];
  }

}
