<?php
/**
 * @file
 * Module that implements two factor authentication using token2.
 */

/**
 * System Settings Form callback: token2_settings().
 *
 * Creates the settings form for the token2 module.
 */
function token2_form_settings($form, &$form_state) {
  $form['token2_host_uri'] = array(
    '#title' => t('Token2 API Hostname'),
    '#type' => 'textfield',
    
    '#default_value' => variable_get('token2_host_uri', 'https://api.token2.com'),
    '#required' => TRUE,
  );

  $form['token2_application'] = array(
    '#type' => 'textfield',
    '#title' => t('token2 Application name'),
    '#default_value' => variable_get('token2_application', 'Token2 for Drupal'),
    '#required' => TRUE,
  );

  $form['token2_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('token2_api_key'),
    '#description' => t('Your application API Key.'),
    '#required' => TRUE,
  );

  
  
 


  $form['token2_allow_sms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow SMS based accounts'),
    '#default_value' => variable_get('token2_allow_sms', '0'),
    '#options' => array(
      '0' => 'no',
      '1' => 'yes'
    ),
    '#description' => t('Allow SMS based authentication? If allowed, users can select between Mobile App and SMS based authentication.'),
    '#required' => FALSE,
  );

  $token2_about = t(
    'Token2 | Hosted two factor authentication platform. Module for Drupal '
  );

  $form['token2_about'] = array(
    '#type' => 'textarea',
    '#title' => t('About text'),
    '#default_value' => variable_get('token2_about', $token2_about),
    '#description' => t('Enter short about text that will be displayed on the token2 configuration page for users.'),
    '#required' => FALSE,
  );

   

  return system_settings_form($form);
}
