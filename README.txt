********************************************************************
                     D R U P A L  -  Token2
********************************************************************
Name: token2
Author: Token2
Drupal: 7
********************************************************************

DESCRIPTION:

Secure your Drupal site with two-factor authentication provided by
Token2. 

With the token2 module installed on your website your users can use
their smart phone as a one-time-password generator, greatly
improving the security of your site.

You can request an one time password from your users not only when
they sign in, but also when they are submitting any forms of your
choice.

********************************************************************
INSTALLATION:

1. Download and install the token2 module, and the Libraries module.

2. Create an account at Token2.com
   
3. Create a new site  at www.token2.com, and copy the API key.

4. Enable the token2 module, and navigate to the token2 module
   configuration page (/admin/config/people/token2).
   
5. Enter the API key from step 3, and the same site name 
   that you used when you created the site at www.token2.com.

6. Create a test drupal account and enable token2 on it by navigating to the
   user page and clicking the token2 tab.
   
********************************************************************
DESIGN CHOICES:

* All users with token2 enabled will be prompted for a token2 token on
  login.
  
* You can add forms you wish to be protected with token2 tokens on
  the configuration page. 

********************************************************************
CONTACT:

* Token2 - https://token2.com/
