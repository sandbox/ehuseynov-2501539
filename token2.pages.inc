<?php
/**
 * @file
 * Module that implements two factor authentication using token2.
 */

/**
 * Menu callback: user/%user/token2.
 *
 * Administer token2 settings for user.
 */
function token2_page_manage($user) {
  module_load_include('inc', 'token2', 'token2.forms');
  $token2_id = _token2_get_token2_id($user->uid, TRUE);
  $token2_enabled = _token2_get_token2_id($user->uid);

  $output = array();

  $output['about'] = array(
    '#markup' => filter_xss(variable_get('token2_about'), array(
      'a',
      'em',
      'strong',
      'cite',
      'blockquote',
      'code',
      'ul',
      'ol',
      'li',
      'dl',
      'dt',
      'dd',
      'br',
      'p',
    )),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  if ($token2_enabled === FALSE) {
    $output['status'] = array(
      '#markup' => t('token2 is not enabled for this Account.'),
      '#prefix' => '<div class="messages warning"><ul><li>',
      '#suffix' => '</ul></div>',
    );

    if ($token2_id !== FALSE) {
      $output['enable_token2'] = drupal_get_form('token2_form_enable', $user);
    }
  }
  else {
    $output['status'] = array(
      '#markup' => t('Token2 is enabled on this account.'),
      '#prefix' => '<div class="messages status"><ul><li>',
      '#suffix' => '</ul></div>',
    );
    $output['disable_token2'] = drupal_get_form('token2_form_disable', $user);
  }

  if ($token2_id === FALSE) {
    $output['register_token2'] = drupal_get_form('token2_form_register', $user);
  }

  return $output;
}